echo on

pushd "C:\Program Files (x86)\Microsoft Visual Studio\2019\BuildTools\VC\Auxiliary\Build"
call vcvarsall.bat %ARCH% -vcvars_ver=%VCVARS_VER%
popd

curl -L "http://www.mirrorservice.org/sites/sources.redhat.com/pub/pthreads-win32/pthreads-w32-%VERS%-release.zip" -o release.zip || goto :error
7z -y x release.zip -osrc || goto :error

set DIST=%CD%\dist
md dist\bin dist\include dist\lib
move Makefile src\pthreads.2

pushd src\pthreads.2
nmake VC-static || goto :error
move pthreadVC2.lib %DIST%\lib\pthreadVC2-s.lib

nmake VC-static-debug || goto :error
move pthreadVC2d.lib %DIST%\lib\pthreadVC2-sd.lib

nmake VC || goto :error
move pthreadVC2.dll %DIST%\bin\pthreadVC2.dll
move pthreadVC2.exp %DIST%\bin\pthreadVC2.exp
move pthreadVC2.ilk %DIST%\bin\pthreadVC2.ilk
move pthreadVC2.lib %DIST%\lib\pthreadVC2.lib
move pthreadVC2.pdb %DIST%\bin\pthreadVC2.pdb

nmake VC-debug || goto :error
move pthreadVC2d.dll %DIST%\bin\pthreadVC2d.dll
move pthreadVC2d.exp %DIST%\bin\pthreadVC2d.exp
move pthreadVC2d.ilk %DIST%\bin\pthreadVC2d.ilk
move pthreadVC2d.lib %DIST%\lib\pthreadVC2d.lib
move pthreadVC2d.pdb %DIST%\bin\pthreadVC2d.pdb

move %DIST%\..\src\Pre-built.2\include\*.* %DIST%\include

popd
md deploy
cd dist
7z a ..\deploy\%DIST_NAME% .

goto :EOF

:error
echo Failed with error #%errorlevel%.
exit /b %errorlevel%
